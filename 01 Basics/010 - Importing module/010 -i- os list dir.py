#-*- coding: utf-8 -*-
#!/usr/bin/env python
#
# Copyright 2018 Mattia Benedetti
# All rights reserved.
#
# Author: Mattia Benedetti

"""
    -   IMPORTING MODULES    -

https://docs.python.org/2.7/library/index.html

"""


import os
print __file__                      # ACTUAL FILE FULL PATH
print __file__.split('\\')[-1]      # ACTUAL FILE NAME
print os.listdir('./')
